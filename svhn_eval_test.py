"""Evaluation for test data

Accuracy:
achieves 94.9% accuracy in test data

"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import math
import time

import numpy as np
import tensorflow as tf

import svhn
import svhn_eval_train
NUM_EXAMPLES_PER_EPOCH_FOR_EVAL = 26032

FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string('eval_dir', 'tmp/svhn_eval_test_logs',
                           """Directory where to write event logs.""")
tf.app.flags.DEFINE_string('eval_data', 'test',
                           """Either 'test' or 'train_eval'.""")
tf.app.flags.DEFINE_string('checkpoint_dir', 'tmp/svhn_train_logs',
                           """Directory where to read model checkpoints.""")
tf.app.flags.DEFINE_integer('eval_interval_secs', 60 * 5,
                            """How often to run the eval.""")
tf.app.flags.DEFINE_integer('num_examples', 26032,
                            """Number of examples to run.""")

def main(argv=None):  # pylint: disable=unused-argument
  
  if tf.gfile.Exists(FLAGS.eval_dir):
    tf.gfile.DeleteRecursively(FLAGS.eval_dir)
  tf.gfile.MakeDirs(FLAGS.eval_dir)
  svhn_eval_train.evaluate('test_batch.bin',num_examples_per_epoch=NUM_EXAMPLES_PER_EPOCH_FOR_EVAL)


if __name__ == '__main__':
  tf.app.run()
