This code is based on the CIFAR10 example code on Tensorflow.

Build the environment for the code:

1. Ubuntu(16.04)
2. tensorflow(0.10.0)
3. anconda(python 2.7,sklearn 0.17.1,numpy 1.11.1)

Introduction some fuctions of each file
- svhn.py 
    - inferenc():Build the SVHN model
    - distorted inputs() and inputs(): input data
- svhn_input.py
    - distorted inputs():construct distorted input for training data using the Reader ops
- svhn_train.py
    - train():train model
- svhn_eval_train.py
    - evaluate():evaluate accuracy of the model in input data

    

Steps to use the code:

1. run maybe_download_and_train_validation_test.py 
    - maybe_download 'train_32x32.mat' and 'test_32x32.mat'
    - split 'train_32x32.mat' into training data and validation data (500 samples per class)
2. run svhn_train.py to train model in training data
    - after 60,000 steps,we will get the model (in tmp/svhn_eval_train_logs) and training/validation accuracy (acc_train_val.npy)
3. run svhn_eval_test.py
    - to get accuracy in test data (test_32x32.mat)
4. run svhn_eval_extra.py
    - to get accuracy in extra_32x32.mat
    - PS:this will replace the test data by extra data


Notable Improvements:
- in svhn_train.py
    - add train/validaion accuray compute to get acc_train_val.npy for model evaluation and validation
- in svhn.py
    - in inference().
        - add function add_conv_pool_norm()
        - add conv3,norm3,pool3
- add maybe_download_and_train_validation_test.py
    - If realted data do not exist,download them;
    - Make a train/validation data splitting and transform to corresponding data type (.bin）;
    - Transform the test data to corresponding data type (.bin)
- add svhn_eval_train.py
    - functions to compute accuracy
- modify svhn_eval.py to svhn_eval_test.py
    - to eval accuracy in test data


Result: after 60k steps,
- train accuracy(in training data):96.1%
- valdation accuracy(in validation data):94.1%
- test accuracy(in test_32x32.mat): 94.9%
- accuracy(in extra_32x32.mat):97.1%
