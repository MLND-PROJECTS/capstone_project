"""Evaluation for training data (68257) and validataion (5000)

Accuracy:
svhn_train.py achieves 96.1% accuracy after 60K steps.

"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import math
import time

import numpy as np
import tensorflow as tf

import svhn
FLAGS = tf.app.flags.FLAGS

def eval_once(saver, summary_writer, top_k_op, summary_op,logits,labels):
  """Run Eval once.

  Args:
    saver: Saver.
    summary_writer: Summary writer.
    top_k_op: Top K op.
    summary_op: Summary op.
  """
  with tf.Session() as sess:
    ckpt = tf.train.get_checkpoint_state(FLAGS.checkpoint_dir)
    if ckpt and ckpt.model_checkpoint_path:
      # Restores from checkpoint
      saver.restore(sess, ckpt.model_checkpoint_path)
      # Assuming model_checkpoint_path looks something like:
      #   /my-favorite-path/svhn_train/model.ckpt-0,
      # extract global_step from it.
      global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
    else:
      print('No checkpoint file found')
      return

    # Start the queue runners.
    coord = tf.train.Coordinator()
    try:
      threads = []
      for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        threads.extend(qr.create_threads(sess, coord=coord, daemon=True,
                                         start=True))

      num_iter = int(math.ceil(FLAGS.num_examples / FLAGS.batch_size))
      true_count = 0  # Counts the number of correct predictions.
      total_sample_count = num_iter * FLAGS.batch_size
      step = 0
      labels_predict_all = np.array([])
      labels_real_all = np.array([])
      while step < num_iter and not coord.should_stop():
        predictions,labels_predict,labels_real = sess.run([top_k_op,logits,labels])
        true_count += np.sum(predictions)
        labels_predict_all = np.append(labels_predict_all,np.array([np.argmax(s) for s in labels_predict]))
        labels_real_all = np.append(labels_real_all,labels_real)
                     
        step += 1

      # Compute precision @ 1.
      precision = true_count / total_sample_count
      print('%s: precision @ 1 = %.3f' % (datetime.now(), precision))
      # record prediction
      
      return precision,labels_predict_all,labels_real_all

      summary = tf.Summary()
      summary.ParseFromString(sess.run(summary_op))
      summary.value.add(tag='Precision @ 1', simple_value=precision)
      summary_writer.add_summary(summary, global_step)
    except Exception as e:  # pylint: disable=broad-except
      coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=10)
     

def evaluate(data_file,num_examples_per_epoch):
  """Eval data for a number of steps."""
  with tf.Graph().as_default() as g:
    # Get images and labels for SVHN.
    images, labels = svhn.inputs(data_file,num_examples_per_epoch)

    # Build a Graph that computes the logits predictions from the
    # inference model.
    logits = svhn.inference(images)

    # Calculate predictions.
    top_k_op = tf.nn.in_top_k(logits, labels, 1)

    # Restore the moving average version of the learned variables for eval.
    variable_averages = tf.train.ExponentialMovingAverage(
        svhn.MOVING_AVERAGE_DECAY)
    variables_to_restore = variable_averages.variables_to_restore()
    saver = tf.train.Saver(variables_to_restore)

    # Build the summary operation based on the TF collection of Summaries.
    summary_op = tf.merge_all_summaries()

    summary_writer = tf.train.SummaryWriter(FLAGS.eval_dir, g)
    
    precision,labels_predict_all,labels_real_all = eval_once(saver, summary_writer, top_k_op, summary_op,logits,labels)
      
    return precision




