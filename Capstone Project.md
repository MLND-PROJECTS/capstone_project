# Capstone Project
## Machine Learning Engineer Nanodegree


## I. Definition
_(approx. 1-2 pages)_

### Project Overview

Computer vision is an interdisciplinary field that deals with how computers can be made to gain high-level understanding from digital images or videos.The Street View House Numbers(SVHN) is a real-world image dataset for developing machine learning and object recognition algorithms with minimal requirement on data preprocessing and formatting.It is similar in format to the popular MNIST dataset (10 digits, 32x32 inputs), but an order of magnitude bigger (600,000 labeled digits), contains color information and various natural backgrounds.
The Street View House Numbers (SVHN) recognition is a common problem in machine learning.SVHN is obtained from house numbers in Google Street View images.The problem is to classify RGB 32x32 pixel images across 10 classes,1 for each digit. 
In this project,I build a model to interpret number strings in real-world imgaes using The Street View House Numbers (SVHN) Dataset and convolutional neural networks(CNN).

### Problem Statement

The goal is to train a model to do digit recognition(in real-word).The problem is to classify RGB 32x32 pixel images across 10 classes so it is a multi-class classification.
Generally,the following algrithms can be used to solve multi-class classification:
- k-nearest neighbours(KNN)
- support vector machine(SVM）
- decision tree,random forest
- neural network(NN)

Convolutional neural networks(CNNs) are a specialized kind of neural network for processing data that have been trenmendously successful in practical applications,especially in digit image recognition.In this project,I will try to build a model using CNNs which can do the digits images recognition .

### Metrics

The model we need is a multi-label classifier.Accuracy is a common metric for multi-label classifier.Because digit recognition is useful only when the model has a high accuracy.So I use accuracy as the metric of the model.

## II. Analysis
_(approx. 2-4 pages)_

### Data Exploration

The Street View House Numbers (SVHN) Dataset is a real-world image dataset for developing machine learning and object recognition algorithms with minimal requirement on data preprocessing and formatting. It can be seen as similar in flavor to MNIST (e.g., the images are of small cropped digits).SVHN is obtained from house numbers in Google Street View images.
SVHN basic information:
- 10 classes, 1 for each digit. Digit '1' has label 1, '9' has label 9 and '0' has label 10.
- 73257 digits for training, 26032 digits for testing, and 531131 additional, somewhat less difficult samples, to use as extra training data
- formats used here:
    MNIST-like 32-by-32 images centered around a single character (many of the images do contain some distractors at the sides).

Every images is as following(RGB images):
![Figure:digits images in SVHN](http://oe3u7oh3a.bkt.clouddn.com/SVHN_digit.jpg 'digits images in SVHN')
### Exploratory Visualization

Label frequencies - trainning set
![Figure:label frequencies](http://upload-images.jianshu.io/upload_images/1875892-13643a7c0b7c5aed.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
The picture shows a class-imbalance problem which may affect the performance of model,but in fact after class-balancing (nearly 5,000 per class),the performance wasn't better.

### Algorithms and Techniques

​
Convolutional Neural Networks(CNNs)​, which is the state-of-the-art algorithm for most image processing tasks, including classification.Comparing to other approaches,they neeed a large amount of training data. Fortunately,SVHN dataset has enough data.One major advantage of CNNs is reducing the number of free parameters and improve generalization compared to usual neural network.And it both reduces memory footprint and improves performance.
Convolutional Neural Networks(CNNs) are very similar to ordinary Neural Networks .CNNs can be considered simply neural networks that use convolution in place of general matrix multiplication in at least one of their layers.And CNNs are made up of layers. Every Layer has a simple API: It transforms an input 3D volume to an output 3D volume with some differentiable function that may or may not have parameters.
Layers used to build CNN and related parameter:
- convolutional layer
    - filter shape
    - stride
    - padding
- pooling layer
    - max pooling shape
- RELU layer
    - shreshhold
- fully connected layer
- loss layer

Other algorithm also had been considered:K-Means 90.6%[^1].

### Benchmark

I swap in the SVHN as the input data of CNN of the CIFAR-10 tutorial[^2],with accuracy(using data 'test_32x32.mat'):94%.I use this as benchmark.

## III. Methodology
_(approx. 3-5 pages)_

### Data Preprocessing

Steps for preprocessing:
- random crop
- whitening
- data augumentation
- shuffle batch

Random crop:
- images data are cropped to 24 x 24 pixels, centrally for evaluation or randomly for training.Random crop can act as a regularizer and base the classification on the presence of parts of the object instead of focusing everything on a very distinct feature that may not always be present.

Whitening:
- images data are approximately whitened to make the model insensitive to dynamic range.

Data augmentation:

- Randomly flip the image from left to right.
- Randomly distort the image brightness.
- Randomly distort the image contrast.

Shuffle batch:
- randomly select a batch of size 128

### Implementation

At first,I used the code of cifar-10 example to train a model with SVHN data (20,000 steps) (train_32x32.mat) and the model got accuracy 94% in test_32x32.After 20,000 steps,the accuracy didn't increase.So I considered to increase the complexity of the model based on the cifar-10 example.
Then,I tried to use a  training/validation/test data-splitting to evaluate and validate the model.I composed my validation set with 2/3 from training samples (400 per class) and 1/3 from extra samples (200 per class), yielding a total of 6,000 samples. This distribution allows to measure success on easy samples but puts more emphasis on difficult ones. The training and testing sets contain respectively 598,388 and 26,032 samples(test_32x32.mat).The training accuracy changing process is very slower than only using 'train_32x32.mat' as training data.After running 20,000 steps,this method achieved a low accuracy (<90%).
Then I used a different training/validation/test data-splitting.I compose my validation set with 5,000 samples (500 per class) from training samples (train_32x32.mat).The training and testing sets contain respectively 68,257(from train_32x32.mat) and 26,032 samples(test_32x32.mat).
In order to increase the complexity,I added a convolutional layer,a RELU layer,a normalization layer and a pooling layer to the code of cifar-10 example.
I added maybe_download_and_train_validation_test.py:
  - If 'train_32x32.mat' or 'test_32x32.mat' don't exist,download them.
  - Implement a training/validation/test data-splitting as mentioned above


##### CNN architecture used
The model here is a multi-layer architecture consisting of alternating convolutions and nonlinearities. These layers are followed by fully connected layers leading into a softmax classifier.
![Figure:CNN architecture illustration](http://upload-images.jianshu.io/upload_images/1875892-771a058291874dad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
where
- CONV:convolutional layer
    - filter shape:5x5
    - stride :1
    - padding:'SAME'
- RELU:rectified linear activation
    - max(features, 0)
- POOL:pooliing layer
    - max_pool:2x2
- NORM:normalization layer
    - local_response_normalization
- FC:fully connected layers

The loss function is softmax-cross-entropy with logits.
The optimizer is GradientDescentOptimizer.


### Refinement

Refinement I had tried respectively:
- add a layer(CONV,RELU,POOL,NORM)
  - Behind the second layer,I added a layer with same parameters as the second layer
- add a layer and dropout
  - Behind the second layer,I added a layer with same parameters as the second layer and dropout (0.8)
- replace GradientDescentOptimizer by AdadeltaOptimizer
- use pooling layer with 4x4 filters (in all two pooling layers)
- solve class-imbalance
  - use under-sampling by removing some examples:randomly select nearly 5,000 samples from every class (> 5,000 samples in train_32x32.mat) so we get totally 50,000 samples as training data (5,000 per class 1~6,nearly 5,000 per other class).

My initial solution:2 CONV layer and 94% accuracy (also is benchmark and 73257 train samples).
After adding a CONV layer (3 CONV layer),the final model get 94.8% accuracy.

## IV. Results
_(approx. 2-3 pages)_

### Model Evaluation and Validation

- The final model got 94.9% accuracy in 'test_32x32.mat' and 97.1% accuracy in 'extra_32x32.mat'.
- Because the model use the data augument,so small perturbations(such as flip the image from left to right and so on) will not affect the results,but reducing training data will affect the results negatively.

The validation set was used to model evaluation and validation with considering loss and train/val accuracy.

Loss

![Figure:loss](http://upload-images.jianshu.io/upload_images/1875892-d65141ebb748d690.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
The loss represents softmax cross entropy between predictions and labels,which measure the probability error in discrete classification tasks.The loss  is evaluated on the individual batches during the forward pass.The shape of loss graph can tell us about the learning rate.From the picture,the loss firstly decrease rapidly (before about 6,000 step) and then decrease very slowly,which shows we get a good learning rate.[^3]


Train/Val accuracy
![Figure:Train/Val accuracy](http://upload-images.jianshu.io/upload_images/1875892-71a57823635aa9b4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
The Train/Val accuracy curve can give us valuable insights into the amount of overfitting in the model.The gap between the training and validation accuracy indicates the amount of overfitting.Before 10,000 step,the gap between training and validation accuracy is very small.After 10,000 step,the gap gradually extends but the difference is under 0.02.So we can consider the model gets little overfitting.[^4]
When I use the whole train_32x32.mat as training data,the performance of model didn't change effectively.So the model is robust enough.
### Justification
The final result gets accuracy 94.9% which is stronger than the benchmark (accuracy 94%).
Using the model in data 'extra_32x32.mat',I got the following results:
- the classification accuracy is 97.1%

## V. Conclusion
### Free-Form Visualization
I applied the model to test data (test_32x32),and then use the predicted and actual labels  to make a confusion matrix.
Confusion_matrix
![Figure:confusion_matrix in test_32x32](http://upload-images.jianshu.io/upload_images/1875892-ee2822e8c2f861c6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
We can see here that the predicts are mostly accurate, with some errors you might expect, e.g., '7' is often confused as '1'.
### Reflection
The process of this project can be summarized as followings:
- I collected and learned materials of deep learning and tensorflow,then I established tensorflow environment.
- I found the cifar-10 example and then compared SVHN dataset with cifar-10 dataset.Then I wrote code to process SVHN dataset so that I could run the cifar-10 example code with SVHN dataset.Then I used the result as benchmark.
- I used data augumentation to invent more data because deep learning algorithms often perform better with more data and made images data whitened to make the model insensitive to dynamic range.
- I made different training/validation/test data-splittings to evaluate and validate the model.
- Because the cifar-10 example code got 94% accuracy and convolutional neural networks have been tremendously successful in image recognition,I decided to use convolutional neural networks to train the model with SVHN dataset.
- Based on the cifar-10 example code,I tried to add layer or dropout,replace GradientDescentOptimizer by AdadeltaOptimizer,select other pooling parameter and solve the class-imbalance.

We can get big wins with changes to training data.Using 'extra_32x32.mat' as training data will lead to training time increase and training performance decrease.Automatic hyperparameter optimization algrorithms (such as grid search) will save much time cost.Since my code is based on cifar-10 code,grasping the code completely as early as possible will make the whole work easy.

### Improvement
To make the model better,we can do as followings
- optimizer train data,modify model to adapt to the error
- try other algrithm such as RCNN(Recurrent Convolutional Neural Network)[^5]
- add more layers and sametime  take care of overfitting(dropout and other techniques can be used)
- get more data to make the performance better 

# References
[^1]:http://yann.lecun.com/exdb/publis/pdf/sermanet-icpr-12.pdf
[^2]:https://www.tensorflow.org/versions/r0.11/tutorials/deep_cnn/index.html#convolutional-neural-networks
[^3]:http://cs231n.github.io/neural-networks-3/#loss
[^4]:http://cs231n.github.io/neural-networks-3/#accuracy
[^5]:http://www.cv-foundation.org/openaccess/content_cvpr_2015/app/2B_004.pdf

------------ -  -
