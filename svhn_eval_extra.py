"""Evaluation for extra data.

Accuracy:
achieves 97.1% accuracy
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import math
import time

import numpy as np
import tensorflow as tf

import svhn
import svhn_eval_train
import os
import sys
from six.moves import urllib
import numpy as np
import scipy.io 
import tensorflow as tf
import pickle
import numpy as np

FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string('eval_dir', 'tmp/svhn_eval_extra_logs',
                           """Directory where to write event logs.""")
tf.app.flags.DEFINE_string('eval_data', 'test',
                           """Either 'test' or 'train_eval'.""")
tf.app.flags.DEFINE_string('checkpoint_dir', 'tmp/svhn_train_logs',
                           """Directory where to read model checkpoints.""")
tf.app.flags.DEFINE_integer('eval_interval_secs', 60 * 5,
                            """How often to run the eval.""")
tf.app.flags.DEFINE_integer('num_examples', 531131,
                            """Number of examples to run.""")

def transform_extra_data(filepath):
    mat = scipy.io.loadmat(filepath)
    data = mat['X']
    label = mat['y']
    np.place(label,label==10,[0]) #replace 10 by 0
    
    R_data = data[:,:,0,:]
    G_data = data[:,:,1,:]
    B_data = data[:,:,2,:]

    R_data = np.transpose(R_data, (2,0,1))
    G_data = np.transpose(G_data, (2,0,1))
    B_data = np.transpose(B_data, (2,0,1))

    R_data = np.reshape(R_data,(data.shape[-1],32*32))
    G_data = np.reshape(G_data,(data.shape[-1],32*32))
    B_data = np.reshape(B_data,(data.shape[-1],32*32))

    outdata = np.concatenate((label,R_data,G_data,B_data), axis = 1)

    if not tf.gfile.Exists('tmp/svhn_data/svhn-batches-bin'):
        tf.gfile.MakeDirs('tmp/svhn_data/svhn-batches-bin')
            
    outdata.tofile('tmp/svhn_data/svhn-batches-bin/extra_batch.bin')
        
    print('save data')

def main(argv=None):  # pylint: disable=unused-argument
  
    dest_directory = 'tmp/svhn_data'
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)

    DATA_URL = 'http://ufldl.stanford.edu/housenumbers/extra_32x32.mat'

    filename = DATA_URL.split('/')[-1]

    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (filename,
              float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')

    transform_extra_data(filepath)
    if tf.gfile.Exists(FLAGS.eval_dir):
        tf.gfile.DeleteRecursively(FLAGS.eval_dir)
    tf.gfile.MakeDirs(FLAGS.eval_dir)
    svhn_eval_train.evaluate('extra_batch.bin',num_examples_per_epoch=531131)


if __name__ == '__main__':
  tf.app.run()
