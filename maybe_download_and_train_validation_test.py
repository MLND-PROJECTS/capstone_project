# coding: utf-8
"""If realted data do not exist,download them;
Make a train/validation data splitting and transform to corresponding data type (.bin）;
Transform the test data to corresponding data type (.bin)
"""
import os
import sys
from six.moves import urllib
import numpy as np
import scipy.io 
import tensorflow as tf
import pickle
import numpy as np



dest_directory = 'tmp/svhn_data'
if not os.path.exists(dest_directory):
    os.makedirs(dest_directory)



def transform_test_data(filepath):
    '''transform filepath to test_batch.bin
    '''
    mat = scipy.io.loadmat(filepath)
    data = mat['X']
    label = mat['y']
    np.place(label,label==10,[0]) #replace 10 by 0
    
    R_data = data[:,:,0,:]
    G_data = data[:,:,1,:]
    B_data = data[:,:,2,:]

    R_data = np.transpose(R_data, (2,0,1))
    G_data = np.transpose(G_data, (2,0,1))
    B_data = np.transpose(B_data, (2,0,1))

    R_data = np.reshape(R_data,(data.shape[-1],32*32))
    G_data = np.reshape(G_data,(data.shape[-1],32*32))
    B_data = np.reshape(B_data,(data.shape[-1],32*32))

    outdata = np.concatenate((label,R_data,G_data,B_data), axis = 1)

    if not tf.gfile.Exists('tmp/svhn_data/svhn-batches-bin'):
        tf.gfile.MakeDirs('tmp/svhn_data/svhn-batches-bin')
            
    outdata.tofile('tmp/svhn_data/svhn-batches-bin/test_batch.bin')
        
    print('save data')



def train_test_split_onelabel(X,y,label,test_size):
    '''for every label,make a train/test splitting
    Args:
        X:narray
        y:narray
        label:int (1~10)
        test_size:int or float(0~1)
    Return:
        X_train,X_test,y_train,y_test
    '''
    # selecting data with label
    con = y.reshape([len(y),]) == label
    labellist = np.arange(len(y))[con]
    new_X = X[:,:,:,labellist]
    
    # split data
    from sklearn.cross_validation import train_test_split
    X_train,X_test,y_train,y_test=train_test_split(
        new_X.reshape((-1,new_X.shape[-1])).T,
        y[labellist],
        test_size=test_size,
        random_state=42)
    
    X_train = X_train.T.reshape((32,32,3,-1))
    X_test = X_test.T.reshape((32,32,3,-1))
    
    return X_train,X_test,y_train,y_test


if __name__ == '__main__':
    DATA_URL = 'http://ufldl.stanford.edu/housenumbers/train_32x32.mat'

    filename = DATA_URL.split('/')[-1]

    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (filename,
              float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')

    train_origin = scipy.io.loadmat('tmp/svhn_data/train_32x32.mat')



    # np.concatenate
    train_X_list = []
    test_X_list = []
    train_y_list = []
    test_y_list = []
    for label in range(1,11):
        # split train_32x32.mat
        X_train,X_test,y_train,y_test = train_test_split_onelabel(train_origin['X'],train_origin['y'],label,test_size=400)
        train_X_list.append(X_train)
        test_X_list.append(X_test)
        train_y_list.append(y_train)
        test_y_list.append(y_test)


    train_X = np.concatenate(train_X_list,axis=3)
    test_X = np.concatenate(test_X_list,axis=3)
    train_y = np.concatenate(train_y_list)
    test_y = np.concatenate(test_y_list)


    

    # transform and save train data
    data = train_X
    label = train_y
    np.place(label,label==10,[0]) #replace 10 by 0

    R_data = data[:,:,0,:]
    G_data = data[:,:,1,:]
    B_data = data[:,:,2,:]

    R_data = np.transpose(R_data, (2,0,1))
    G_data = np.transpose(G_data, (2,0,1))
    B_data = np.transpose(B_data, (2,0,1))

    R_data = np.reshape(R_data,(data.shape[-1],32*32))
    G_data = np.reshape(G_data,(data.shape[-1],32*32))
    B_data = np.reshape(B_data,(data.shape[-1],32*32))

    outdata = np.concatenate((label,R_data,G_data,B_data), axis = 1)


    if not tf.gfile.Exists('tmp/svhn_data/svhn-batches-bin'):
        tf.gfile.MakeDirs('tmp/svhn_data/svhn-batches-bin')


    outdata.tofile('tmp/svhn_data/svhn-batches-bin/train_batch.bin')
    print('save data')


    # trainsform and save validation data
    data = test_X
    label = test_y
    np.place(label,label==10,[0]) #replace 10 by 0

    R_data = data[:,:,0,:]
    G_data = data[:,:,1,:]
    B_data = data[:,:,2,:]

    R_data = np.transpose(R_data, (2,0,1))
    G_data = np.transpose(G_data, (2,0,1))
    B_data = np.transpose(B_data, (2,0,1))

    R_data = np.reshape(R_data,(data.shape[-1],32*32))
    G_data = np.reshape(G_data,(data.shape[-1],32*32))
    B_data = np.reshape(B_data,(data.shape[-1],32*32))

    outdata = np.concatenate((label,R_data,G_data,B_data), axis = 1)


    if not tf.gfile.Exists('tmp/svhn_data/svhn-batches-bin'):
        tf.gfile.MakeDirs('tmp/svhn_data/svhn-batches-bin')


    outdata.tofile('tmp/svhn_data/svhn-batches-bin/validation_batch.bin')
    print('save data')


    #maybe_download and transform_test_data
    DATA_URL = 'http://ufldl.stanford.edu/housenumbers/test_32x32.mat'

    filename = DATA_URL.split('/')[-1]

    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):
        def _progress(count, block_size, total_size):
            sys.stdout.write('\r>> Downloading %s %.1f%%' % (filename,
              float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')

    transform_test_data(filepath)
